var notas = document.querySelector("#notas");
let armazenamento = document.querySelector(".box_valores");
var contador = 1; //vai contar quantas notas foram passadas
var lista = []; //vai armazenar as notas


// função para armazenar as notas e para mostrar as notas na tela
function armazenar_notas() {

    notas.value = notas.value.replace(",","."); //Transforma virgula em ponto

    // if para verificar se as notas estão entre 0 e 10 e se é número ou não
    if(parseFloat(notas.value)>=0 && parseFloat(notas.value)<=10 && !isNaN(notas.value)==true) {
        
        armazenamento.append("A nota " + contador + " foi: " + notas.value + "\n");
        contador = contador + 1;
        lista.push(parseFloat(notas.value)); //Vai armazenar no formato float
    }

    // Condição caso notas não estejam entre o e 10 ou se não for número
    else if(parseFloat(notas.value)<0 || parseFloat(notas.value)>10 || !isNaN(notas.value)==false){
        
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    }

    // Condição caso não seja impresso nada no input ou caso seja colocado espaço
    else {
        
        alert("Por favor, insira uma nota.");
    }

    notas.value = "";

}

// função para calcular a média das notas
function calcular_media() {

    let mostrarMedia = document.querySelector(".resultado_final p");
    mostrarMedia.innerHTML = "A Média é: ";

    // Condição caso aperte "calcular média" antes de adicionar nota
    if(contador == 1) {

        var media = 0;

    }

    else {

        var soma = lista.reduce(function(acumulador, valorAtual,) {
            return acumulador + valorAtual;
        },0);

        var media = (soma/(contador-1)).toFixed(2); //Arredondando para duas casas decimais
    }

    //Imprimindo o resultado na tela
    mostrarMedia.append(media);
}

// Adicionando as funções aos botões
let button_adicionar = document.querySelector(".input_notas button");
button_adicionar.addEventListener("click", armazenar_notas);

let button_calcular = document.querySelector(".resultado_final button");
button_calcular.addEventListener("click", calcular_media);